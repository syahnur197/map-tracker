
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import * as VueGoogleMaps from 'vue2-google-maps'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Vue.use(VueGoogleMaps, {
//     load: {
//       key: 'AIzaSyDDc-WxYq_SfrI9gnm--RJ3NVSaTii0Tq8'
//     },
//   })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data() {
        return {
            latitude: 4.5353,
            longitude: 114.7277,
            position: {
                lat: 4.5353,
                lng: 114.7277,
                alt: 0
            },
            watchIds: [],
            markersLineCoordinates: [],
            map: null,
            markers: []
        }
    },
    mounted() {
        Echo.channel('location')
            .listen('SendLocation', (e) => {
                console.log('SendLocation event received!');
                this.data = e.location;
                const watchId = e.watchId;
                console.log(this.watchIds.includes(watchId));
                this.updateMap(this.data, watchId);
            })

        this.launchMap(this.latitude, this.longitude);
    },
    methods: {
        launchMap(lat, lng) {
            var brunei = {lat: lat, lng: lng}
            this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: brunei,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            // this.marker = new google.maps.Marker({
            //     map : this.map,
            //     animation : 'bouncer',
            // })
            // this.marker.setMap(this.map);
            // this.lineCoordinates.push(new google.maps.LatLng(this.latitude, this.longitude));
        },

        updateMap(data, watchId) {
            this.position.lat = parseFloat(data.latitude)
            this.position.lng = parseFloat(data.longitude)

            this.map.setCenter(this.position);

            console.log(watchId);

            if(this.watchIds.includes(watchId)) { // if existing markers
                const index = this.watchIds.indexOf(watchId);
                this.markers[index].setPosition(this.position);
                this.markersLineCoordinates[index].push(new google.maps.LatLng(this.position.lat, this.position.lng));
            } else { // if new marker
                this.watchIds.push(watchId)
                // add new marker to markers
                const newMarker = new google.maps.Marker({
                    map : this.map,
                    animation : 'bouncer',
                    visible : true
                });
                this.markers.push(newMarker);
                this.markersLineCoordinates.push([new google.maps.LatLng(this.position.lat, this.position.lng)]);
            }

            var lineCoordinatesPath = new google.maps.Polyline({
                path: this.lineCoordinates,
                geodesic: true,
                map: this.map,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
              });
        }
    }
});


