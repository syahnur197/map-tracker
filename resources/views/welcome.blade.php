<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDc-WxYq_SfrI9gnm--RJ3NVSaTii0Tq8"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                height: 100%;
                padding: 0;
            }
            #map {
                height: 100%;
                position : static !important;
            }
        </style>
    </head>
    <body>
        <div id="app" >
            {{-- <gmap-map ref="mapRef"
                :center="{lat:latitude, lng:longitude}"
                :zoom="7"
                map-type-id="terrain"
                style="width: 500px; height: 300px"
              >
              <gmap-marker
                    :position="position"
                    :clickable="true"
                    :draggable="true"
                    @click="center=m.position"
                />
            </gmap-map> --}}
            <div id="map"></div>
        </div>
    </body>
    <script src="{{ asset('js/app.js')}}"></script>
</html>
